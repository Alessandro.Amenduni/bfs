window.onload = function () {

    let cy = cytoscape({

        container: document.getElementById('cy'), // container to render in
        // container: $('#cy'),


        style: [// the stylesheet for the graph
            {
                selector: 'node',
                style: {
                    'background-color': '#666',
                    'label': 'data(id)'
                }
            },

            {
                selector: 'edge',
                style: {
                    'width': 2,
                    'line-color': '#ccc',
                    'curve-style': 'bezier', // .. and target arrow works properly
                    'target-arrow-color': '#ccc',
                    'target-arrow-shape': 'triangle'
                }
            }
        ],

        // see http://js.cytoscape.org/#layouts
        layout: {
            name: 'grid',
            rows: 2,
            cols: 4
        }
    });

    var Nnodi = 0;
    var nodi = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'z'];
    var part = prompt("inserisci il nodo da cui vuoi partire");
    var NodoPartenza = part;
    var NumPartenza;
    for (let i = 0; i < nodi.length; i++) {
        if(nodi[i] == NodoPartenza){
             NumPartenza = i;
			 console.log("trovato");
        }
    }
    var Narchi = 0;
    var matrice = [];
    for (let i = 0; i < Nnodi; i++) {
        matrice[i] = new Array(Nnodi);
    }

    function bfs() {
        let c = 1;
        var arr = [];
        arr = visitato();
        for (let i = 0; i < arr.length; i++) {
            if (i == 0) {
                cy.elements('node#' + nodi[arr[i]]).style({'background-color': 'blue'});
            } else {
                cy.elements('node#' + nodi[arr[i]]).style({'background-color': 'yellow'});
            }
            for (let j = 0; j < Nnodi; j++) {
                if (matrice[arr[i]][j] == '1' && j == arr[c]) {
                    cy.elements('edge#' + nodi[arr[i]] + nodi[arr[c]]).style({'line-color': 'yellow'});
                    c++;
                }
            }
        }

    }

    function visitato() {
        let c = 0;
        var finale = [];
        var arr = [];
        arr = RitornoArray();
        var visita = [Nnodi]; //array booleano tutto false lo dichiaro successivamente
        for (let i = 0; i < Nnodi; i++) {
            visita[i] = false;
        }
        for (let i = 0; i < arr.length; i++) { //se la pos nell'array della funzione 
            if (visita[arr[i]] == false) {    // RitoronoArray() è falso aggiungo al'arr finale
                finale[c] = arr[i];
                visita[arr[i]] = true;
                c++;
            }
        }
        return finale;
    }


    function RitornoArray() {
        var c = 1;
        var cont = 1;
        var arr = [];
        arr[0] = NumPartenza;
        for (var i = 0; i < Nnodi * 2; i++) {
            for (var k = 0; k < Nnodi; k++) {
                if (matrice[NumPartenza][k] == '1') {
                    arr[c] = k;
                    c++;
                }
            }
            NumPartenza = arr[cont];
            cont++;
        }
        return arr;
    }


    document.getElementById("btn1").addEventListener("click", function () {
        let options = {name: 'random', animate: true};
        let layout = cy.layout(options)
        layout.run();
    });


    document.getElementById("btn2").addEventListener("click", function () {
        var frase = prompt("Quanti nodi vuoi");
        Nnodi = parseInt(frase);
        for (let i = 0; i < Nnodi; i++) {
            node = cy.add({
                group: 'nodes',
                data: {id: nodi[i]},
                position: {x: 200, y: 200}
            });
        }
    });

    document.getElementById("btn3").addEventListener("click", function () {
        for (let i = 0; i < Nnodi; i++) {
            matrice[i] = new Array(Nnodi);
        }
        var frase = prompt("Quanti archi vuoi");
        Narchi = parseInt(frase);

        for (let i = 0; i < Narchi; i++) {
            let num = Math.floor(Math.random() * Nnodi);
            let num1 = Math.floor(Math.random() * Nnodi);
            if (num == num1) {
                num1 = Math.floor(Math.random() * Nnodi);
            }
            if (matrice[num][num1] == '1') {
                i--;
            }
            matrice[num][num1] = '1';
            cy.add([{
                    group: 'edges',
                    data: {id: nodi[num] + nodi[num1], source: nodi[num], target: nodi[num1]}
                }
            ]);
        }
    });


    document.getElementById("btn4").addEventListener("click", function () {
        bfs();
    });

    document.getElementById("btn6").addEventListener("click", function () {
        for (let i = 0; i < Nnodi; i++) {
            $("table").append("<tr></tr>");
            for (let k = 0; k < Nnodi; k++) {
               if (matrice[i][k] != '1') {
                    matrice[i][k] = '0';
                }
                $("table").append("<td>" + matrice[i][k] + "</td>");
            }
        }
    });
}




